import os
import sys
import asyncio
import aiohttp
import numpy as np
import pandas as pd
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import ALL_COMPLETED
from settings import *
from db_query import InstitutionDB, DoctorInfopanelDB, DoctorsDB, PatientsListDB
import random
import tempfile
import datetime
import json
import time as TIME
from pymongo import MongoClient
from settings import DB, COLLECTIONS


class Scheduler:
    """Интерфейс получения расписания.

    ОТвечает за загрузку всех организация инициирование запуска расписания
    для каждого из Institute, формирование и агрегирование расписания через
    Report. После этого работает на запись

    """

    # кол-во одновременных задач в лупе
    SEMAPHORE = 10
    SEMAPHORE_UPDATE = 40

    def __init__(self):
        self.loop = asyncio.get_event_loop()
        self.pool = ThreadPoolExecutor(max_workers=8)
        pd.options.mode.chained_assignment = None

    def __call__(self, depth=14):
        org_list = self.loop.run_until_complete(self.get_institutions())
        self.loop.run_until_complete(self.fetch_institution(org_list))

    async def fetch_institution(self, org_list):
        semaphore = asyncio.Semaphore(value=self.SEMAPHORE, loop=self.loop)
        sem_up = asyncio.Semaphore(value=self.SEMAPHORE_UPDATE, loop=self.loop)
        tasks = [
            Institution.run(item, semaphore, self.loop, self.pool)
            for item in org_list
            if item.get("qms_id", None)
        ]
        done, _ = await asyncio.wait(tasks, return_when=ALL_COMPLETED)
        result = []
        for item in done:
            res = item.result()
            if res:
                result.extend(res)
        if result:
            async with PatientsListDB() as conn:
                await conn.remove_schedule({"doctor": {'$in' : [item['doctor'] for item in result]}, "date":{'$in' : [item['date'] for item in result]} })
                #import ipdb; ipdb.set_trace()
                await conn.save_scheduler(result)
            #client = MongoClient('10.80.99.17', 27017)
            #db = client.WaitingList
            #collection = db.schedule
            #res_id = collection.delete_many({"doctor": {'$in' : [item['doctor'] for item in result]}})
            #res_id = collection.delete_many({"doctor": {'$in' : [item['doctor'] for item in result]}, "date":{'$in' : [item['date'] for item in result]} })
            #res_id = collection.insert_many(result)
            #client.close()
            #client = MongoClient(**DB["default"])
            """
            tasks = [self.update_scheduler(row, sem_up) for row in result]
            await asyncio.wait(tasks, return_when=ALL_COMPLETED)
            """
        else:
            print("Нечего писать")

    async def get_institutions(self):
        """Получение списка доступных организаций"""
        async with InstitutionDB() as conn:
            qms_ids = await conn.get_qms_id_list()
            org_list = await conn.qms_org_list(qms_ids)
        return org_list

    async def update_scheduler(self, flt, row, semaphore):
        async with semaphore:
            async with PatientsListDB() as conn:
                await conn.update_scheduler(flt, row)


class PrepareDataframe:
    """Методы для обработки датафреймов"""

    @classmethod
    def pretreatment_scheduler(cls, df, doctors):
        """Предобработка полученныух данных с расписанием
        
        Обрабатываем поля расписания, фильтруем по списку докторов

        """
        try:
            df['pAz'] = df['pAz'].str.replace('_', ' ')
            df['puR'] = df['puR'].str.replace('_', ' ')
            df_doctors = pd.DataFrame.from_records(doctors)
            df.rename(index=str, columns={
                'pAz': 'doctor_fullname',
                'RSpID': 'squad',
                'pMO': 'cabinet',
                'puR': 'specialty'
            }, inplace=True)
            #df.dropna(subset=["doctor_fullname"], inplace=True)
            df_doctors['doctor_fullname'] = df_doctors['name'].map(
                lambda x: f"{x.get('last', '')} {x.get('first', '')} {x.get('middle', '')}" if isinstance(x, dict) else x)
            df_doctors["squad"] = df_doctors["squad"].astype(str)
            df["squad"] = df["squad"].astype(str)
            df = df.merge(df_doctors,
                on=['squad', 'doctor_fullname', 'specialty'], how="inner")
            df["organization"] = df["organization"].map(
                lambda x: x.get("reestr", np.nan))
            if 'login' not in df.columns:
                df['login'] = "undefined"
            df.dropna(subset=['_id', 'login', 'organization'], inplace=True)
            df.rename(index=str, columns={'_id': 'doctor'}, inplace=True)
            df["slots"] = df["days"].map(lambda x: x[0].get('slots', np.nan))
            df.dropna(subset=["slots"], inplace=True)
            if df.empty:
                return df
            # TODO: возможно здесь вместо удаления столбца cabinet стоит из
            # него удалить NaN
            df = df[['squad', 'n656', 'doctor_fullname', 'doctor', 'filial',
                'organization', 'slots']]
            """
            df.drop(["name", "password", "days", "cabinet_x", "cabinet_y"],
                axis=1, inplace=True)
            """
            columns = df.drop('slots', axis=1).columns.tolist()
            count_field = len(columns)
            df = df.groupby(columns).slots.apply(
                lambda x: pd.DataFrame(x.values[0])).reset_index().drop(
                f'level_{count_field}', axis = 1)
        except Exception as e:
            print(f"pretreatment_scheduler: {e}")
            #import ipdb;ipdb.set_trace()
            df = pd.DataFrame()
        return df

    @classmethod
    def prepare_data(cls, df, date):
        """Подготовка данных для последующей записи в БД"""
        try:
            # убираем нулевое время
            df = df[df["time"].str.startswith("00:")==False]
            # избавляемся от словарей в patients
            if df.empty:
                return df
            if "patients" not in df.columns:
                df["patients"] = np.nan
            df["patients"] = df["patients"].apply(lambda x: 1 if isinstance(x, (list, dict)) else x)
            records_empty = df[df["patients"].isna()].groupby(
                ["doctor"])["patients"].size()
            records_total = df.groupby(["doctor"])["patients"].size()
            #df = df[df["patients"]==1.0]
            df["time.begin"] = df["time"].map(
                lambda x: (datetime.datetime.strptime(f"{date}-{x.split('-')[0]}", "%Y%m%d-%H:%M")))
            df["time.begin"] = df["time.begin"].map(lambda x: TIME.mktime(x.utctimetuple()))
            df["time.begin"] = df["time.begin"].map(lambda x: datetime.datetime.utcfromtimestamp(x))
            df["time.end"] = df["time"].map(
                lambda x: (datetime.datetime.strptime(f"{date}-{x.split('-')[1]}", "%Y%m%d-%H:%M")))
            df["time.end"] = df["time.end"].map(lambda x: TIME.mktime(x.utctimetuple()))
            df["time.end"] = df["time.end"].map(lambda x: datetime.datetime.utcfromtimestamp(x))
            df["date"] = datetime.datetime.strptime(f"{date}", "%Y%m%d")
            df = df[["organization", "doctor", "date", "time.begin", "time.end"]]
            df["time"] = df[["time.begin", "time.end"]].apply(
                lambda x: {"begin": x["time.begin"], "end": x["time.end"]}, axis=1)
            df = df.groupby(["organization", "doctor", "date"])["time"].apply(list).reset_index()
            df = df.merge(
                pd.DataFrame({"doctor": records_empty.index, "empty": records_empty.values}),
                on="doctor", how="outer")
            df = df.merge(
                pd.DataFrame({"doctor": records_total.index, "total": records_total.values}),
                on="doctor", how="outer")
            df.fillna(0, inplace=True)
            df["empty"] = df["empty"].astype(int)
            df["total"] = df["total"].astype(int)
            df["doctor"] = df["doctor"].map(lambda x: str(x))
        except Exception as e:
            print(f"prepare_data: {e}")
            df = pd.DataFrame()
        return df


class Institution:
    """Универсальный интерфейс для организации"""

    PATH_TO_TEMP = "tempfiles"
    URL_QMS_REQUEST = URL_QMS_REQUEST
    #DEPTH = DEPTH_SCHEDULER
    DEPTH = 14
    # размер блоков, получаемы при запросе расписания qms
    QMS_CHUNK_SIZE = 128

    @classmethod
    async def fetch(cls, semaphore, date, qms_id):
        headers = {
            "Content-type": "application/json",  # Определение типа данных
            "Accept": "text/plain",
            "Content-Encoding": "utf-8",
        }
        url = cls.URL_QMS_REQUEST
        data = {"qqc235": qms_id, "date": date}
        async with semaphore:
            async with aiohttp.ClientSession() as session:
                async with session.post(
                    url,
                    data=json.dumps(data, ensure_ascii=False).encode("utf-8"),
                    headers=headers,
                    ssl=False,
                ) as resp:
                    with tempfile.NamedTemporaryFile(
                        delete=False,
                        dir=cls.PATH_TO_TEMP,
                        suffix=".json",
                        prefix=f"{qms_id}_",
                    ) as f:
                        file_path = f.name
                        while True:
                            chunk = await resp.content.read(cls.QMS_CHUNK_SIZE)
                            if not chunk:
                                break
                            f.write(chunk)
        return file_path

    def json_to_dataframe(tempfile):
        try:
            data = json.load(open(tempfile))
            df = pd.DataFrame.from_records(data["data"])
        except Exception as e:
            df = pd.DataFrame()
            print(f"json_to_dataframe, {e}")
        if tempfile and os.path.exists(tempfile):
            os.remove(tempfile)
        return df

    @classmethod
    async def get_scheduler(cls, org, doctors, date, semaphore, loop, pool):
        tempfile = await cls.fetch(semaphore, date, org["qms_id"])
        df = await loop.run_in_executor(pool, cls.json_to_dataframe, *(tempfile,))
        if df.empty:
            return None
        df = await loop.run_in_executor(
            pool, PrepareDataframe.pretreatment_scheduler, *(df,doctors,))
        if df.empty:
            return None
        df = await loop.run_in_executor(
            pool, PrepareDataframe.prepare_data, *(df, date,))
        if df.empty:
            return None
        return df.to_dict("records")

    @classmethod
    async def run(cls, org, semaphore, loop, pool):
        """
        Формат org:
        {'_id': ObjectId('5cb3e7bde818fdf14b0983f0'), 'name':, 'shortName': ',
        'reestr': '240139', 'monitoring': {}, 'filials': [], 'squads': [],
        'qms_id': 'vAE', 'roles': [{'title': 'Администратор', 'filials': [],
        'sections': [{'title': 'Пользователи', 'icon': 'user-md', 'component':
        'Users', 'edit': True}, {'title': 'Очереди', 'icon': 'h-square',
        'component': 'Queues', 'edit': True}, {'title': 'Пульты',
        'icon': 'calculator', 'component': 'Panels', 'edit': True},
        {'title': 'Табло', 'icon': 'television', 'component': 'Tables', 'edit': True},
        {'title': 'Терминалы', 'icon': 'tablet', 'component': 'Terminals', 'edit': True},
        {'title': 'Администраторы', 'icon': 'user', 'component': 'Administrators', 'edit': True}],
        """
        dates = pd.date_range(pd.datetime.today(), periods=cls.DEPTH)
        dates = dates.map(lambda x: x.strftime("%Y%m%d"))
        async with DoctorInfopanelDB() as conn:
            doctors = await conn.get_doctors(org["reestr"])
        if not doctors:
            return None
        doctors_ids = [
            item
            for doc in doctors
            for item in doc.get("publishers", {}).get("users", [])
        ]
        async with DoctorsDB() as conn:
            doctors = await conn.get_doctors_by_id(doctors_ids)

        coros = [
            cls.get_scheduler(org, doctors, date, semaphore, loop, pool)
            for date in dates
        ]
        done, _ = await asyncio.wait(coros, return_when=ALL_COMPLETED)
        result = []
        for item in done:
            res = item.result()
            if res:
                result.extend(res)
        return result


if __name__ == "__main__":
    sched = Scheduler()
    sched()
