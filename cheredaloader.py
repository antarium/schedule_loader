"""Классы для работы с расписанием"""


import os
import numpy as np
import pandas as pd
import datetime
import json
import asyncio
import aiohttp
from settings import batch_log, sgrid_log
from messages import *


class BatchScheduler:
    """Родительский класс для батчей всех видов расписания

    Аргументы класса:
        - sgreed -- объект, хранящий расписание, тип объекта переопределяется
        в дочерних классах и зависит от специфики данных

    """
    log = batch_log
    sgreed = None

    def __init__(self, date, loop, pool=None):
        """Конструктор класса
        
        Аргументы:
            - date::<str> -- дата за которую получаем расписание,
                формат данных yyyymmdd
            - loop -- экземпляр event-loop
            - pool -- экземпляр ThreadPoolExecutor

        """
        if len(date) != 8:
            self.log.warning(f"{BATCH_DATE_FORMAT_ERROR} получено: {date}")
        self.date = date
        self.loop = loop
        self.pool = pool

    def __repr__(self):
        return f"{self.__class__}, date:{self.date}"


class BatchSchedulerQMS(BatchScheduler):
    # TODO: где-то на этом этапе нужно получать squads и reestr_code, подробнее prepare.py rows:152-153

    def __init__(self, qms_code, date, loop, pool=None):
        self.qms_code = qms_code
        super(BatchSchedulerQMS, self).__init__(date, loop, pool)
        self.sgreed = SGridQMS()

    async def get_batch_schedule(self):
        """Получить расписание"""
        pass

    def __repr__(self):
        parent = super(BatchSchedulerQMS, self).__repr__()
        return f"{parent}, organization code: qms_{self.qms_code}"


class SGridDF:
    """Родительский класс для всех реализаций сетки расписания, на DataFrame"""
    log = sgrid_log
    df = pd.DataFrame()

    def __call__(self, path_to_data, delete_file=True):
        """collable метод класса
        
        Аргументы:
            - path_to_data::<str> -- путь к источнику данных, в текущей
                реализации предполагается JSON-сериализованный файл
            - delete_file::<bool> -- удалить или нет файл с исходными данными

        При вызове метода делается попытка прочитать сериализованный JSON
        и провести необходимую обработку. Кодировка файла path_to_data UTF8

        """
        # гарантируем очистку датафрейма
        self.df = pd.DataFrame()
        if not os.path.exists(path_to_data):
            self.log.warning(f"{ERROR_EXISTS_FILE}, файл: {path_to_data}")
            return False
        try:
            data = json.load(open(path_to_data))
            if data.get('data', None):
                #здесь преобразуем данные в Dataframe 
                self.df = pd.DataFrame.from_records(data['data'])
            else:
                self.log.warning(f"{JSON_NOT_DATA}, файл: {path_to_data}")
        except Exception as e:
            self.log.warning(
                f"{ERROR_JSON_READ}, ошибка {e}, файл: {path_to_data}")
        finally:
            if delete_file:
                os.remove(path_to_data)


class SGridQMS(SGridDF):
    """Реализация сетки расписания qMS"""

    ALIASES_FIELDS = {
        'pAz': 'doctor_fullname',
        'RSpID': 'squad',
        'pMO': 'cabinet',
        'puR': 'specialty'
    }

    def split_docname(self, line):
        fio = ['last', 'first', 'middle']
        res = dict(zip(fio, line.split(' ', 2)))
        return {"name.{}".format(k): v for k, v in res.items()}

    def make_query(self, docname, squad, org_reestr, specialty):
            docname.update({
                'squad': squad,
                'organization.reestr': org_reestr,
                'specialty': specialty
            })
            return docname
    
    def pretreatment_df(self, squads, reestr_code):
        """Прадварительная обработка датафрейма

        Дейтсвия:
            - фильтруем данные по перечню допустимых специальностей (squads)
            - получаем из поля days массивы со слотами и массивы с датами
            - удаляем строки с пустыми слотами и датами
            - в полях pAz и puR заменяем _ на пробел, так как там хранятся
            ФИО специалистов
            - Переименовываем поля согласно ALIAS_FIELDS, для совместимости
            с полями MongoDB
            - Создаем поле doc.name в котором формируем словарь с ФИО врачей
            - Формируем поле с данными для поиска врачей в MongoDB

        Возвращает True/False в зависимости от успеха всех операций

        """
        df = self.df
        try:
            df['RSpID'] = df['RSpID'].apply(lambda x: str(x))
            df = df[df['RSpID'].isin(squads)]
            # потрошим поле days (тип данных dict)
            df['slots'] = df['days'].map(lambda x: x[0].get('slots', np.nan))
            df['date'] = df['days'].map(lambda x: x[0].get('date', np.nan))
            # TODO: здесь посмотреть для вычисления времени слота
            df.dropna(subset=['slots', 'date'], inplace=True)
            df['reestr'] = reestr_code
            # предобработка, переименовываем поля, приводим к нужному типу
            df.dropna(subset=["pAz"], inplace=True)
            df[["pAz", "puR"]] = df[["pAz", "puR"]].applymap(
                lambda x: x.replace("_", " ") if not pd.isna(x) else x)
            df.rename(index=str, columns=self.ALIASES_FIELDS, inplace=True)
            df['doc.name'] = df['doctor_fullname'].map(
                lambda x: self.split_docname(x))
            # формируем поле для поиска докторов
            vec_make_query = np.frompyfunc(self.make_query, 4, 1)
            df['filter_find'] = vec_make_query(df['doc.name'], df['squad'],
                df['reestr'], df['specialty'])
        except Exception as e:
            self.log.warning(f"{ERROR_PREPARING_DF_SCHEDULER}, ошибка: {e}")
            return False
        else:
            self.df = df
        return True

    def rest_to_dataframe(self, rest):
        """
          преобразуем массив словарей в датафрейм, поле users (там словари)
          разбиваем на отдельные строки
        """
        try:
            df = pd.DataFrame.from_records(rest)
            df.dropna(subset=['users'], inplace=True)
            df['interval_begin'] = df['interval'].map(
                lambda x: x.get('begin', 0))
            df['interval_end'] = df['interval'].map(lambda x: x.get('end', 0))
            df.drop(columns=['interval', '_id', 'squads'], inplace=True)
            columns = df.drop('users', axis=1).columns.tolist()
            count_field = len(columns)
            df= df.groupby(columns).users.\
                apply(lambda x: pd.DataFrame(x.values[0])).reset_index().\
                drop(f'level_{count_field}', axis = 1)
            columns.append('users')
            df.columns = columns
        except Exception as e:
            self.log.warning(f"{ERROR_PREPARING_DF_SCHEDULER2}, ошибка: {e}")
            return False
        return df

    def posttreatment_scheduler(self, doctors_from_db, rest_list):
        """Обработка расписания

        Подготовка данных для внесения в БД (MongoDB)

        Аргументы:
            - doctors_from_db::<list & records> -- список докторов из БД для
            фильтра в self.df
            - rest_list::<list & records> -- список исключений времени в
            расписании докторов

        """
        df_rest = self.rest_to_dataframe(rest_list)
        if df_rest == False:
            return False
        df = self.df
        try:
            df_from_db = pd.DataFrame.from_records(doctors_from_db)

        except Exception as e:
            self.log.warning(f"{ERROR_PREPARING_DF_SCHEDULER2}, ошибка: {e}")
            return False
        else:
            self.df = df
        return True


class SGridTimeTableQMS(SGridDF):
    """Реализация сетки расписания из qMS для timetable"""
    pass


