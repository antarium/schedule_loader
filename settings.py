"""Файл настроек"""


import logging



PATH_TO_LOG = "loader.log"
LOG_FORMAT = "%(asctime)s - %(name)s: %(filename)s[LINE:%(lineno)d]# %(levelname)-8s  %(message)s"

fh = logging.FileHandler(PATH_TO_LOG)
formatter = logging.Formatter(LOG_FORMAT)
fh.setFormatter(formatter)
batch_log = logging.getLogger("loader.Batch")
batch_log.addHandler(fh)
batch_log.setLevel(logging.DEBUG)

sgrid_log = logging.getLogger("loader.SGrid")
sgrid_log.addHandler(fh)
sgrid_log.setLevel(logging.DEBUG)

# лог времени выполнения программы
fh2 = logging.FileHandler("runtime.log")
formatter = logging.Formatter("%(asctime)s: %(message)s")
fh2.setFormatter(formatter)
runtime_log = logging.getLogger()
runtime_log.addHandler(fh2)
runtime_log.setLevel(logging.INFO)