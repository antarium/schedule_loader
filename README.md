# Платформа для актуализации расписания сервисов "Череда" и МР

Отвечает за сбор расписания из МИС организаций работающих с сервисами "Череда" и МР и предоставление форматированного расписания этим сервисам. 
Предоставляет API для управления очередями задач платформы по сети TCP/IP, а также за API для предоставления расписания сервису МР

Работа платформы обеспечивается тремя независимыми процессами (желательно наличие хотя бы двухядерного процессора):
- Очередь задач (он же главный или управляющий процесс): в рамках собственного event-loop организует работу веб-вервер, отвечающий за прием и размещение в очереди управляющих команд от клиента. Он же инициализирует два PIPELINE с каждым из двух других процессов
- Процесс, отвечающий за актуализацию расписания для сервиса "Череда", согласно управляющим командам из pipeline
- Процесс, отвечающий за актуализацию расписания для сервиса МР, согласно управляющим командам из pipeline

Команды, доступные из клиента:
- Получение статуса, запрос: -s (--service) <chereda/ms> status варианты ответов: обработка задания, запущен
- Получение данных лога, запрос: -s (--service) <chereda/ms> log <n> (n-количество последних строк, по умолчанию 20) ответ: массив строк
- Обновить скрипты сервиса, запрос: -s (--service) <chereda/ms> update варианты ответов: now (скрипт будет переподгружен сейчас), later (выполняется задание, после него скрипт будет перезапущен)
- Запустить задание на обновление расписания: -s (--service) <chereda/ms> -o (--orgs) <all(по умолчанию), либо reestr_code организации> варианты ответов: now (задание будет запущено сейчас), later (выполняется задание, после него скрипт будет перезапущен)

## Из важного:
- pAz обязательно должно быть не пустым, так как именно из него берем doctor_fullname


