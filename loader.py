"""Отвечает за сбор расписания из МИС организаций работающих с 
сервисами "Череда" и МР и предоставление форматированного 
расписания этим сервисам. Предоставляет API для управления очередями
задач платформы по сети TCP/IP, а также за API для предоставления
расписания сервису МР""" 


from multiprocessing import Process, Pipe, cpu_count
from concurrent.futures import ThreadPoolExecutor
import asyncio
import time
import random


async def test_func(num, pool):
	loop = asyncio.get_event_loop()
	t_sleep = random.randrange(1,5)
	print(f"{num}: время сна: {t_sleep}")
	await asyncio.sleep(t_sleep)
	t_sleep = random.randrange(1,5)
	print(f"{num}: второе время сна: {t_sleep}")
	await loop.run_in_executor(pool, time.sleep, t_sleep)
	print(f"{num} готово")


if __name__ == "__main__":
	pool = ThreadPoolExecutor(max_workers=cpu_count())
	t_start = time.time()
	loop = asyncio.get_event_loop()
	nums = [1,2,3,4]
	tasks = [test_func(num, pool) for num in nums]
	tasks = asyncio.wait(tasks)
	loop.run_until_complete(tasks)
	t_end = time.time()
	print(t_end-t_start)