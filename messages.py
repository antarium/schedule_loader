"""Константы с текстами сообщений"""


BATCH_DATE_FORMAT_ERROR = """Формат данных для атрибута date не удовлетворяет
требуемому"""

ERROR_EXISTS_FILE = "Файл не найден"
ERROR_DATA_EMPTY = "Данные не обнаружены"
JSON_NOT_DATA = "В JSON не обрнаружен ключ data"
ERROR_JSON_READ = "Ошибка чтения JSON"
ERROR_PREPARING_DF_SCHEDULER = "Ошибка ПРЕДобработки фрейма с расписанием"
ERROR_PREPARING_DF_SCHEDULER2 = "Ошибка ПОСТобработки фрейма с расписанием"