import asyncio
import motor.motor_asyncio
from settings import DB, COLLECTIONS
from bson.objectid import ObjectId


class CheredaDB:
    DB_CONN = DB["default"]
    COLLECTIONS = COLLECTIONS

    async def __aenter__(self):
        """
          Открываем соединение
        """
        self.client = motor.motor_asyncio.AsyncIOMotorClient(
            self.DB_CONN.get('url', 'localhost'), self.DB_CONN.get('port', 27017))
        self.db = self.client[self.DB_CONN['name']]
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        """
          Закрываем подключение.
        """
        self.client.close()
        if exc_val:
            raise

    async def by_filtering(self, collect, filt=None, out=None):
        """
          делает выборку find()
          collect - коллекция из которой делать выборку 
          filt - условия фильтра, если пусто, то без фильтра
          out - ключ, по которому выводить информацию, если 
          None, то выводить все
        """
        collection = self.db[collect]
        cursor = collection.find(filt)
        if out:
            result = [doc[out] async for doc in cursor if doc.get(out, None)]
        else:
            result = [doc async for doc in cursor]
        return result

    async def remove_collections(self, collect, for_delete=None):
        collection = self.db[collect]
        result = await collection.delete_many(for_delete)

    async def insert_data(self, collect, data):
        collection = self.db[collect]
        result = await collection.insert_many(data)

    async def update_data(self, collect, flt, data):
        collection = self.db[collect]
        result = await coll.update_one(flt, {'$set': data})


class InstitutionDB(CheredaDB):
    """
      надстройка для работы сколлекцией Организации
      запускать через менеджер контекста, потому что просто через __init__
      не инициализируется connection
    """

    def __init__(self):
        self.COL_NAME = self.COLLECTIONS['org']

    async def get_qms_id_list(self):
        KEY = 'qms_id'
        result = await self.by_filtering(self.COL_NAME, out = KEY)
        return result

    async def qms_org_list(self, ids_list):
        """
          формируем список всех документов, у которых qms_id not None,
          тоесть всех, кто работает в QMS
        """
        result = await self.by_filtering(self.COL_NAME, 
            filt={'qms_id': {'$in': ids_list}})
        return result


class DoctorInfopanelDB(CheredaDB):
    """Получение списка докторов для инфопанели"""

    def __init__(self):
        self.COL_NAME = self.COLLECTIONS['interfaces']

    async def get_doctors(self, reestr):
        result = await self.by_filtering(self.COL_NAME, 
            filt={'organization': reestr, 'type': "infopanel",
                "deleted" : False})
        return result


class DoctorsDB(CheredaDB):
    """
      надстройка для работы сколлекцией doctors
      запускать через менеджер контекста, потому что просто через __init__
      не инициализируется connection
    """

    def __init__(self):
        self.COL_NAME = self.COLLECTIONS['doctors']

    async def get_doctors_by_id(self, ids):
        ids = [ObjectId(item) for item in ids]
        result = await self.by_filtering(self.COL_NAME,
            filt={'_id': {'$in': ids}})
        return result


class PatientsListDB(CheredaDB):

    def __init__(self):
        self.COL_NAME = self.COLLECTIONS['schedule']

    async def remove_schedule(self, data):
        await self.remove_collections(self.COL_NAME, data)

    async def save_scheduler(self, data):
        await self.insert_data(self.COL_NAME, data)

    async def update_scheduler(self, flt, data):
        await self.update_data(self.COL_NAME, flt, data)




